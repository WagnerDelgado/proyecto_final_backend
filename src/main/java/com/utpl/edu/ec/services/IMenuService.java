package com.utpl.edu.ec.services;

import java.util.List;

import com.utpl.edu.ec.model.Menu;

public interface IMenuService extends ICRUD<Menu, Integer>{
	
	List<Menu> listarMenuPorUsuario(String nombre);

}
