package com.utpl.edu.ec.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="medico")
public class Medico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdMedico;
	
	@Column(name = "cedula", nullable=false, length=13, unique=true)
	private String cedula;
	
	@Column(name = "nombres", nullable=false, length=100)
	private String nombres;
	
	@Column(name = "apellidos", nullable=false, length=100)
	private String apellidos;

	@ManyToOne
	@JoinColumn(name = "id_especialidad", nullable = false, foreignKey = @ForeignKey(name = "FK_medico_especialidad"))
	private Especialidad especialidad;
	
	@ManyToOne
	@JoinColumn(name = "id_horario", nullable = false, foreignKey = @ForeignKey(name = "FK_medico_horario"))
	private Horario horario;

	public Integer getIdMedico() {
		return IdMedico;
	}

	public void setIdMedico(Integer idMedico) {
		IdMedico = idMedico;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}
}
