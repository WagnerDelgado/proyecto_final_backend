package com.utpl.edu.ec.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="especialidad")
public class Especialidad {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdEspecialidad;
	
	@Column(name = "nombre", nullable=false, length=100, unique = true)
	private String nombre;
	
	@Column(name = "descripcion", nullable=false, length=100)
	private String descripcion;

	public Integer getIdEspecialidad() {
		return IdEspecialidad;
	}

	public void setIdEspecialidad(Integer idEspecialidad) {
		IdEspecialidad = idEspecialidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
