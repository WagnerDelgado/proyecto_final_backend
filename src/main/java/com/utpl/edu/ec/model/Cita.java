package com.utpl.edu.ec.model;

import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="cita")
public class Cita {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_cita;
	
	@Column(name = "fecha_cita", nullable = false)
	private LocalDateTime fecha_cita;
	
	@Column(name = "hora_cita", nullable = false, unique = true)
	private String hora_cita;
	
	@ManyToOne
	@JoinColumn(name = "IdPaciente", nullable = false, foreignKey = @ForeignKey(name = "FK_cita_paciente"))
	private Paciente paciente;
	
	@ManyToOne
	@JoinColumn(name = "IdMedico", nullable = false, foreignKey = @ForeignKey(name = "FK_cita_medico"))
	private Medico medico;

	public Integer getId_cita() {
		return id_cita;
	}

	public void setId_cita(Integer id_cita) {
		this.id_cita = id_cita;
	}

	public LocalDateTime getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(LocalDateTime fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public String getHora_cita() {
		return hora_cita;
	}

	public void setHora_cita(String hora_cita) {
		this.hora_cita = hora_cita;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	
}
