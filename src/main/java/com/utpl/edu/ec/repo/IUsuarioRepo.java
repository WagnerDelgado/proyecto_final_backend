package com.utpl.edu.ec.repo;

import com.utpl.edu.ec.model.Usuario;

public interface IUsuarioRepo extends IGenericRepo<Usuario, Integer>  {

	//from usuario where username = ?
	//Derived Query
	Usuario findOneByUsername(String username);	
}
