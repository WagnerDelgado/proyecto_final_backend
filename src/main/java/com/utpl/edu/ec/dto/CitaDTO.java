package com.utpl.edu.ec.dto;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.utpl.edu.ec.model.Medico;
import com.utpl.edu.ec.model.Paciente;

public class CitaDTO {
	
	private Integer id_cita;
	@NotNull
	private LocalDateTime fecha_cita;
	@NotNull
	private String hora_cita;
	@NotNull
	private Paciente paciente;
	@NotNull
	private Medico medico;
	public Integer getId_cita() {
		return id_cita;
	}
	public void setId_cita(Integer id_cita) {
		this.id_cita = id_cita;
	}
	public LocalDateTime getFecha_cita() {
		return fecha_cita;
	}
	public void setFecha_cita(LocalDateTime fecha_cita) {
		this.fecha_cita = fecha_cita;
	}
	public String getHora_cita() {
		return hora_cita;
	}
	public void setHora_cita(String hora_cita) {
		this.hora_cita = hora_cita;
	}
	public Paciente getPaciente() {
		return paciente;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	
}
