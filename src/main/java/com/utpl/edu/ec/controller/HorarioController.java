package com.utpl.edu.ec.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.utpl.edu.ec.dto.HorarioDTO;
import com.utpl.edu.ec.exception.ModeloNotFoundException;
import com.utpl.edu.ec.model.Horario;
import com.utpl.edu.ec.services.IHorarioService;


@RestController
@RequestMapping("/horarios")
public class HorarioController {
	
	@Autowired
	private IHorarioService service;
	
	@Autowired
	private ModelMapper mapper;

	@GetMapping
	public ResponseEntity<List<HorarioDTO>> listar() throws Exception{
		List<HorarioDTO> lista = service.listar().stream().map(p -> mapper.map(p, HorarioDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	//@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<HorarioDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		HorarioDTO dtoResponse;
		Horario obj = service.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, HorarioDTO.class);
		}
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
	}
	
	/*@PostMapping
	public ResponseEntity<EspecialidadDTO> registrar(@Valid @RequestBody EspecialidadDTO dtoRequest) throws Exception {
		Especialidad p = mapper.map(dtoRequest, Especialidad.class);
		Especialidad obj = service.registrar(p);
		EspecialidadDTO dtoResponse = mapper.map(obj, EspecialidadDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED);
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody HorarioDTO dtoRequest) throws Exception {
		Horario p = mapper.map(dtoRequest, Horario.class);
		Horario obj = service.registrar(p);
		HorarioDTO dtoResponse = mapper.map(obj, HorarioDTO.class);
		//localhost:8080/Especialidads/9
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dtoResponse.getIdHorario()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<HorarioDTO> modificar(@Valid @RequestBody HorarioDTO dtoRequest) throws Exception {
		Horario h = service.listarPorId(dtoRequest.getIdHorario());
		
		if(h == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdHorario());	
		}		
		
		Horario p = mapper.map(dtoRequest, Horario.class);		 
		Horario obj = service.modificar(p);		
		HorarioDTO dtoResponse = mapper.map(obj, HorarioDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Horario pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
