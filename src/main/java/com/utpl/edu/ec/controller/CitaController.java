package com.utpl.edu.ec.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.utpl.edu.ec.dto.CitaDTO;
import com.utpl.edu.ec.dto.MedicoDTO;
import com.utpl.edu.ec.exception.ModeloNotFoundException;
import com.utpl.edu.ec.model.Cita;
import com.utpl.edu.ec.model.Medico;
import com.utpl.edu.ec.services.ICitaService;
import com.utpl.edu.ec.services.IMedicoService;

@RestController
@RequestMapping("/citas")
public class CitaController {
	
	@Autowired
	private ICitaService service;
	
	@Autowired
	private ModelMapper mapper;

	@GetMapping
	public ResponseEntity<List<CitaDTO>> listar() throws Exception{
		List<CitaDTO> lista = service.listar().stream().map(p -> mapper.map(p, CitaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	//@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<CitaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		CitaDTO dtoResponse;
		Cita obj = service.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, CitaDTO.class);
		}
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
	}
	
	/*@PostMapping
	public ResponseEntity<MedicoDTO> registrar(@Valid @RequestBody MedicoDTO dtoRequest) throws Exception {
		Medico p = mapper.map(dtoRequest, Medico.class);
		Medico obj = service.registrar(p);
		MedicoDTO dtoResponse = mapper.map(obj, MedicoDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED);
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody CitaDTO dtoRequest) throws Exception {
		Cita p = mapper.map(dtoRequest, Cita.class);
		Cita obj = service.registrar(p);
		CitaDTO dtoResponse = mapper.map(obj, CitaDTO.class);
		//localhost:8080/Medicos/9
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dtoResponse.getId_cita()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<CitaDTO> modificar(@Valid @RequestBody CitaDTO dtoRequest) throws Exception {
		Cita pac = service.listarPorId(dtoRequest.getId_cita());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getId_cita());	
		}		
		
		Cita p = mapper.map(dtoRequest, Cita.class);		 
		Cita obj = service.modificar(p);		
		CitaDTO dtoResponse = mapper.map(obj, CitaDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Cita pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	
	@GetMapping("/hateoas/{id}")
	public EntityModel<CitaDTO> listarHateoas (@PathVariable("id") Integer id) throws Exception{
		Cita obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		CitaDTO dto = mapper.map(obj, CitaDTO.class);
		
		EntityModel<CitaDTO> recurso = EntityModel.of(dto);
		//localhost:8080/Medicos/1
		WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
		
		recurso.add(link1.withRel("cita-link"));
		return recurso;
	}

}
