package com.utpl.edu.ec.service.Impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.utpl.edu.ec.model.Paciente;
import com.utpl.edu.ec.repo.IGenericRepo;
import com.utpl.edu.ec.repo.IPacienteRepo;
import com.utpl.edu.ec.services.IPacienteService;

@Service
public class PacienteServiceImpl extends CRUDImpl<Paciente, Integer> implements IPacienteService{

	@Autowired
	private IPacienteRepo repo;

	@Override
	protected IGenericRepo<Paciente, Integer> getRepo() {
		return repo;
	}
	
}
