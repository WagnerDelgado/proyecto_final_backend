package com.utpl.edu.ec.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utpl.edu.ec.model.Horario;
import com.utpl.edu.ec.repo.IGenericRepo;
import com.utpl.edu.ec.repo.IHorarioRepo;
import com.utpl.edu.ec.repo.IMedicoRepo;
import com.utpl.edu.ec.services.IHorarioService;

@Service
public class HorarioServiceImpl extends CRUDImpl<Horario, Integer> implements IHorarioService{

	@Autowired
	private IHorarioRepo repo;
	
	@Override
	protected IGenericRepo<Horario, Integer> getRepo() {
		return repo;
	}

}
